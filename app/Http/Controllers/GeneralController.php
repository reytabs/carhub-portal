<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GeneralController extends Controller
{
    public function index()
    {
        return view('general.index');
    }

    public function listingDetails()
    {
        return view('general.listing-details');
    }

    public function listings()
    {
        return view('general.listings');
    }

    public function aboutUs()
    {
        return view('general.about-us');
    }

    public function contactUs()
    {
        return view('general.contact-us');
    }

    public function sellCar()
    {
        return view('general.car-sell');
    }
}
