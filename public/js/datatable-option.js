$('#users').DataTable({
    'paging'      : true,
    'lengthChange': false,
    'searching'   : true,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : false,
    "pageLength": 30,
    "order": [[ 0, "desc" ]]
  })