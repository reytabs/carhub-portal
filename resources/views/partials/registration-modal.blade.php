<!--Register-Form -->
<div class="modal fade" id="signupform">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Sign Up</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="signup_wrap">
                        <div class="col-md-6 col-sm-6">
                            <!-- <form action="#" method="post" id="registration-submit"> -->
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Firstname" name="firstname" id="firstname">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Lastname" name="lastname" id="lastname">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Username" name="username" id="username">
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Email Address" name="email" id="email">
                                </div>
                               
                                <!-- <div class="form-group checkbox">
                                    <input type="checkbox" id="terms_agree">
                                    <label for="terms_agree">I Agree with <a href="#">Terms and Conditions</a></label>
                                </div> -->
                                
                            <!-- </form> -->
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Mobile No." name="mobile" id="mobile">
                            </div>
                            <div class="form-group">
                                <select class="form-control" name="role" id="role">
                                    <option value="buyer">Buyer</option>
                                    <option value="seller">Seller</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" placeholder="Password" name="password" id="password">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" placeholder="Confirm Password" name="password_confirmation" id="password_confirmation">
                            </div>
                            
                            <!-- <h6 class="gray_text">Login the Quick Way</h6>
                            <a href="#" class="btn btn-block facebook-btn"><i class="fa fa-facebook-square" aria-hidden="true"></i> Login with Facebook</a> <a href="#" class="btn btn-block twitter-btn"><i class="fa fa-twitter-square" aria-hidden="true"></i> Login with Twitter</a> <a href="#" class="btn btn-block googleplus-btn"><i class="fa fa-google-plus-square" aria-hidden="true"></i> Login with Google+</a>  -->
                        </div>
                        <!-- <div class="form-group"> -->
                            <button id="registration-submit" class="btn btn-block">Sign Up</button>
                        <!-- </div> -->
                        <!-- <div class="mid_divider"></div> -->
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center">
                <p>Already got an account? <a href="#loginform" data-toggle="modal" data-dismiss="modal">Login Here</a></p>
            </div>
        </div>
    </div>
</div>
<!--/Register-Form --> 