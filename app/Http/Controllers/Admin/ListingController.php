<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Vehicle;

class ListingController extends Controller
{
    private $vehicleModel; 

    public function __construct(Vehicle $vehicleModel)
    {
        $this->vehicleModel = $vehicleModel;
    }

    public function listings()
    {
        $vehicles = $this->vehicleModel->all();
        
        return view('cms.listings.index', compact('vehicles'));
    }
}
