<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	DB::table('roles')->insert([
    		'name' => 'admin',
    		'display_name' => 'Administrator',
    		'description' => 'All Access',
            'created_at' => new DateTime,
            'updated_at' => new DateTime
    	]);

        DB::table('roles')->insert([
            'name' => 'seller',
            'display_name' => 'Seller',
            'description' => 'Seller Access',
            'created_at' => new DateTime,
            'updated_at' => new DateTime
        ]);

    	DB::table('roles')->insert([
    		'name' => 'buyer',
    		'display_name' => 'Buyer',
    		'description' => 'Buyer Access',
            'created_at' => new DateTime,
            'updated_at' => new DateTime
    	]);
    }
}
