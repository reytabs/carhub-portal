<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class SessionController extends Controller
{
    public function __construct(User $userModel)
    {
        $this->userModel = $userModel;
    }
    public function login(Request $request)
    {
        return view('cms.auth.login');
    }

    public function postLogin(Request $request)
    {
        if (\Auth::attempt(['username' => $request->username, 'password' => $request->password])) {
            return redirect()->route('admin.dashboard');
        } else {
            return redirect()->back()->with('login_message', 'Invalid username/password. Please try again!');
        }
    }

    public function logout()
    {
        \Auth::logout();

        return redirect()->route('admin.login');
    }
}
