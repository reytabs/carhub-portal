<!DOCTYPE HTML>
<html lang="en">
    <head>
        @include('blocks.metadata')
        <link rel="stylesheet" href="{{ asset('css/main.css') }}" type="text/css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" type="text/css">
        <script type="text/javascript">
            //<![CDATA[
            var APPLICATION_URL = "{{ url('/') }}";
            var CSRF_TOKEN      = "{{ csrf_token() }}";
            //]]>
        </script>
        @yield('styles')
    </head>
    <body>
        <!--Header-->
        <header class="header_style2 nav-stacked affix-top" data-spy="affix" data-offset-top="1">
            @include('blocks.main-header')
        </header>
        <!-- /Header --> 
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- <div id="app"> -->
            @yield('content')
            <!-- </div> -->
        </div>
        <!-- /.content-wrapper -->
        <!--Footer -->
        <footer>
            @include('blocks.main-footer')
        </footer>
        <!-- /Footer--> 
        <!--Back to top-->
        <div id="back-top" class="back-top"> <a href="#top"><i class="fa fa-angle-up" aria-hidden="true"></i> </a> </div>
        <!--/Back to top--> 
        @include('partials.login-modal')
        @include('partials.registration-modal')
        @include('partials.forgotpassword-modal')
        <!-- Scripts --> 
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script> 
        <script src="assets/js/interface.js"></script> 
        <!--bootstrap-slider-JS--> 
        <script src="assets/js/bootstrap-slider.min.js"></script> 
        <!--Slider-JS--> 
        <script src="assets/js/slick.min.js"></script> 
        <script src="assets/js/owl.carousel.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
        <script>
            $('#loginBtn').click(function () {
                var data = {
                    username: $('#loginUsername').val(),
                    password: $('#loginPassword').val()
                };
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    type: "POST",
                    url: '/login',
                    data: data,
                    success: function(result) {
                        if(!result.success) {
                            toastr.error(result.message);
                        } else {
                            window.location.reload();
                        }
                    }, 
                    error: function (res) {
                        var errors = res.responseJSON.errors;
                        Object.keys(errors).forEach(function (key) {
                            var messages = errors[key].join("\n");
                            toastr.error(messages, key.charAt(0).toUpperCase() + key.slice(1));
                        });
                    }
                });
            });
            $('#registration-submit').click(function () {
                var data = {
                    firstname: $('#firstname').val(),
                    lastname: $('#lastname').val(),
                    username: $('#username').val(),
                    email: $('#email').val(),
                    mobile: $('#mobile').val(),
                    password: $('#password').val(),
                    password_confirmation: $('#password_confirmation').val(),
                    role: $('#role').val(),
                };
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    type: "POST",
                    url: '/registration',
                    data: data,
                    success: function(result) {
                        toastr.success('Registered Successfully. You can now login with your account!');
                        $('#signupform').modal('hide');
                        $('#loginform').modal('show');
                        $('#firstname').val(''),
                        $('#lastname').val(''),
                        $('#username').val(''),
                        $('#email').val(''),
                        $('#mobile').val(''),
                        $('#password').val(''),
                        $('#role').val('')
                    }, 
                    error: function (res) {
                        var errors = res.responseJSON.errors;
                        Object.keys(errors).forEach(function (key) {
                            var messages = errors[key].join("\n");
                            toastr.error(messages, key.charAt(0).toUpperCase() + key.slice(1));
                        });
                    }
                });
            });
        </script>
        @yield('script')
    </body>
</html>