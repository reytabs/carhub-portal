<?php

use Illuminate\Database\Seeder;

class CreateAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = DB::table('users')->insertGetId([
    		'username' => 'administrator',
    		'email' => 'administrator@gmail.com',
    		'password' => \Hash::make('password'),
        ]);
        DB::table('user_details')->insert([
            'user_id' => $user,
    		'firstname' => 'administrator',
    		'lastname' => 'administrator@gmail.com',
    		'mobile' => 123456789,
        ]);

        DB::table('role_user')->insert([
            'user_id' => $user,
    		'role_id' => 1
        ]);

    }
}
