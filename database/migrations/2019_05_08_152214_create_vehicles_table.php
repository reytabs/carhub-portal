<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('brand');
            $table->string('model');
            $table->integer('mileage');
            $table->enum('transmission', ['automatic', 'manual']);
            $table->enum('fuel_type', ['Gasoline', 'Diesel', 'Liquified Petroleum', 'Compressed Natural Gas', 'Ethanol', 'Bio-diesel']);
            $table->string('engine');
            $table->integer('seats');
            $table->dateTime('registered_year');
            $table->dateTime('ownership_license_in_years');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
