@extends('layout.cms')
@section('content')

    
<section class="content-header">
    <h1>
        Sellers
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Sellers</a></li>
        <!-- <li><a href="#">My Account</a></li> -->
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
        <div class="box">
            <div class="box-header">
              <!-- <h3 class="box-title">Data Table With Full Features</h3> -->
            </div>
            
            <!-- /.box-header -->
            <div class="col-sm-12">
                @if(session()->has('success'))
                    <div class="callout callout-success">
                        {{ session('success') }}
                    </div>
                @endif
                @if(session()->has('error'))
                    <div class="callout callout-danger">
                        {{ session('error') }}
                    </div>
                @endif
                
                <!-- <button type="submit" class="btn bg-purple btn-flat margin pull-right">Submit</button> -->
            </div>  
            <div class="box-body">
                <!-- <div class="col-md-6" style="padding: 0; margin-bottom: 20px;">
                    <a href="" class="btn btn-sm btn-primary">Create Sellers</a>
                </div> -->
                
                <table id="users" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Fullname</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($sellerList as $user)
                            <tr>
                                <td><a href="{{ route('sellers.edit', ['id' => $user->id]) }}">{{ $user->detail->firstname }} {{ $user->detail->lastname }}</a></td>
                                <td>{{ $user->username }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->detail->mobile }}</td>
                                <td>
                                    <a href="{{ route('sellers.edit', ['id' => $user->id]) }}" class="btn bg-primary btn-xs btn-flat margin" style="margin: 0;">Edit</a>
                                    <a href="{{ route('sellers.delete', ['id' => $user->id]) }}" class="btn bg-maroon btn-xs btn-flat margin" style="margin: 0;">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
    </div>
</section>
@endsection