@extends('layout.main')
@section('content')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/contact-us.css') }}">
@endsection

<!--Page Header-->
<section class="page-header contactus_page">
    <div class="container">
        <div class="page-header_wrap">
            <div class="page-heading">
                <h1>Contact Us</h1>
            </div>
            <ul class="coustom-breadcrumb">
                <li><a href="#">Home</a></li>
                <li>Contact Us</li>
            </ul>
        </div>
    </div>
    <!-- Dark Overlay-->
    <div class="dark-overlay"></div>
</section>
<!-- /Page Header--> 
<!--Contact-us-->
<section class="contact_us section-padding">
    <div class="container">
        <div  class="row">
            <div class="col-md-6">
                <h3>Your Enquiry / Feedback</h3>
                <!-- <div class="contact_form gray-bg"> -->
                    <form action="#" method="get">
                        <div class="form-group">
                            <label class="control-label">Full Name <span>*</span></label>
                            <input type="text" class="form-control white_bg" id="fullname">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Email Address <span>*</span></label>
                            <input type="email" class="form-control white_bg" id="emailaddress">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Phone Number <span>*</span></label>
                            <input type="text" class="form-control white_bg" id="phonenumber">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Message <span>*</span></label>
                            <textarea class="form-control white_bg" rows="4"></textarea>
                        </div>
                        <div class="form-group">
                            <button class="btn" type="submit">Send Message <span class="angle_arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></span></button>
                        </div>
                    </form>
                <!-- </div> -->
            </div>
            <div class="col-md-6">
                <h3>Contact Info</h3>
                    <div id="accordion" class="checkout">
                        <div class="panel checkout-step">
                            <div> <span class="checkout-step-number">1</span>
                                <h4 class="checkout-step-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" > Singapore</a></h4>
                            </div>
                            <div id="collapseOne" class="collapse in">
                                <div class="checkout-step-body">
                                    <div>Ntegrator Pte Ltd</div>
                                    <div>4 Leng Kee Road #06-04,</div>
                                    <div>SIS Building,</div>
                                    <div>Singapore 159088</div>
                                    <br>
                                    <div>Tel: (65) 6479 6033</div>
                                    <div>Fax: (65) 6472 2966</div>
                                    <div>Email: salesenquiry@ntegrator.com</div>
                                </div>
                            </div>
                        </div>
                        <div class="panel checkout-step">
                            <div role="tab" id="headingTwo"> <span class="checkout-step-number">2</span>
                                <h4 class="checkout-step-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" > Myanmar </a> </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="checkout-step-body">
                                    <div>Ntegrator Pte Ltd</div>
                                    <div>4 Leng Kee Road #06-04,</div>
                                    <div>SIS Building,</div>
                                    <div>Singapore 159088</div>
                                    <br>
                                    <div>Tel: (65) 6479 6033</div>
                                    <div>Fax: (65) 6472 2966</div>
                                    <div>Email: salesenquiry@ntegrator.com</div>
                                </div>
                            </div>
                        </div>
                        <div class="panel checkout-step">
                            <div role="tab" id="headingThree"> <span class="checkout-step-number">3</span>
                                <h4 class="checkout-step-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree"  > Philippines </a> </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="checkout-step-body">
                                    <div>Ntegrator Pte Ltd</div>
                                    <div>4 Leng Kee Road #06-04,</div>
                                    <div>SIS Building,</div>
                                    <div>Singapore 159088</div>
                                    <br>
                                    <div>Tel: (65) 6479 6033</div>
                                    <div>Fax: (65) 6472 2966</div>
                                    <div>Email: salesenquiry@ntegrator.com</div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</section>
<!-- /Contact-us--> 
<!--Brands-->
<section class="brand-section gray-bg">
    <div class="container">
        <div class="brand-hadding">
            <h5>Popular Brands</h5>
        </div>
        <div class="brand-logo-list">
            <div id="popular_brands">
                <div><a href="#"><img src="assets/images/100x60.png" class="img-responsive" alt="image"></a></div>
                <div><a href="#"><img src="assets/images/100x60.png" class="img-responsive" alt="image"></a></div>
                <div><a href="#"><img src="assets/images/100x60.png" class="img-responsive" alt="image"></a></div>
                <div><a href="#"><img src="assets/images/100x60.png" class="img-responsive" alt="image"></a></div>
                <div><a href="#"><img src="assets/images/100x60.png" class="img-responsive" alt="image"></a></div>
                <div><a href="#"><img src="assets/images/100x60.png" class="img-responsive" alt="image"></a></div>
                <div><a href="#"><img src="assets/images/100x60.png" class="img-responsive" alt="image"></a></div>
            </div>
        </div>
    </div>
</section>
<!-- /Brands-->
@endsection