<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Http\Requests\UpdateSellerRequest;
use App\UserDetail;

class UserController extends Controller
{
    private $userModel;
    private $userDetailModel;

    public function __construct(User $userModel, UserDetail $userDetailModel)
    {
        $this->userModel = $userModel;
        $this->userDetailModel = $userDetailModel;
    }
    public function sellerList()
    {
        $sellerList = $this->userModel->whereHas('roles', function ($q) {
            $q->where('name', 'seller');
        })->get();

        return view('cms.sellers.index', compact('sellerList'));
    }

    public function editSeller($id)
    {
        $user = $this->userModel->find($id);
        return view('cms.sellers.edit', compact('user'));
    }

    public function updateSeller($id, UpdateSellerRequest $request)
    {
        $user = $this->userModel->find($id);
        $user->username = $request->username;
        $user->email = $request->email;
        $user->save();

        $userDetail = $this->userDetailModel->where('user_id', $id)->first();
        $userDetail->firstname = $request->firstname;
        $userDetail->lastname = $request->lastname;
        $userDetail->mobile = $request->mobile;
        $userDetail->save();

        return redirect()->back()->with('success', 'Seller successfully updated.');
    }

    public function deleteSeller($id)
    {
        $user = $this->userModel->find($id)->delete();

        return redirect()->back()->with('success', 'Seller successfully deleted.'); 
    }

    public function buyerList()
    {
        $buyerLists = $this->userModel->whereHas('roles', function ($q) {
            $q->where('name', 'buyer');
        })->get();

        return view('cms.buyers.index', compact('buyerLists'));
    }

    public function editBuyer($id)
    {
        $user = $this->userModel->find($id);
        return view('cms.buyers.edit', compact('user'));
    }

    public function updateBuyer($id, UpdateSellerRequest $request)
    {
        $user = $this->userModel->find($id);
        $user->username = $request->username;
        $user->email = $request->email;
        $user->save();

        $userDetail = $this->userDetailModel->where('user_id', $id)->first();
        $userDetail->firstname = $request->firstname;
        $userDetail->lastname = $request->lastname;
        $userDetail->mobile = $request->mobile;
        $userDetail->save();

        return redirect()->back()->with('success', 'Buyer successfully updated.');
    }

    public function deleteBuyer($id)
    {
        $user = $this->userModel->find($id)->delete();

        return redirect()->back()->with('success', 'Buyer successfully deleted.'); 
    }

}
