<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('blocks.cms.metadata')
        <script type="text/javascript">
            //<![CDATA[
            var APPLICATION_URL = "{{ url('/') }}";
            var CSRF_TOKEN      = "{{ csrf_token() }}";
            //]]>
        </script>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <header class="main-header">
                @include('blocks.cms.main-header')
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                @include('blocks.cms.main-sidebar')
            </aside>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- <div id="app"> -->
                @yield('content')
                <!-- </div> -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                @include('blocks.cms.main-footer')
            </footer>
            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                @include('blocks.cms.control-sidebar-dark')
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
                immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        </div>
        @include('blocks.cms.assets')
        @yield('additional-scripts')
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/1.0.7/js/dataTables.responsive.min.js"></script>
    </body>
</html>