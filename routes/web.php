<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'GeneralController@index')->name('general');

Route::get('/listing-details', 'GeneralController@listingDetails')->name('listing-details');

Route::get('listings', 'GeneralController@listings')->name('listings');

Route::get('about-us', 'GeneralController@aboutUs')->name('about-us');

Route::get('contact-us', 'GeneralController@contactUs')->name('contact-us');

Route::get('sell-car', 'GeneralController@sellCar')->name('sell-car');

Route::get('profile', 'UserController@profile')->name('profile');

Route::get('my-listing', 'UserController@myListings')->name('myListings');

Route::get('post-listing', 'UserController@postListing')->name('postListing');

Route::post('login', 'UserController@login');

Route::post('registration', 'UserController@registration');

Route::get('logout', 'UserController@logout')->name('logout');

Route::patch('profile/update/{id}', 'UserController@profileUpdate')->name('update.profile');

Route::group(['prefix' => 'admin'], function () {
    Route::get('login', 'Admin\SessionController@login')->name('admin.login');
    Route::post('login', 'Admin\SessionController@postLogin')->name('post.login');
    Route::get('logout', 'Admin\SessionController@logout')->name('logout');
});

Route::group(['middleware' => 'auth'], function(){
    Route::group(['middleware' => ['role:admin']], function() {
        Route::group(['prefix' => 'admin'], function () {
            Route::get('dashboard', 'Admin\IndexController@index')->name('admin.dashboard');
    
            Route::group(['prefix' => 'sellers'], function () {
                Route::get('/', 'Admin\UserController@sellerList')->name('sellers.index');
                Route::get('edit/{id}', 'Admin\UserController@editSeller')->name('sellers.edit');
                Route::patch('update/{id}', 'Admin\UserController@updateSeller')->name('sellers.update');
                Route::get('delete/{id}', 'Admin\UserController@deleteSeller')->name('sellers.delete');
            });

            Route::group(['prefix' => 'buyers'], function () {
                Route::get('/', 'Admin\UserController@buyerList')->name('buyers.index');
                Route::get('edit/{id}', 'Admin\UserController@editBuyer')->name('buyers.edit');
                Route::patch('update/{id}', 'Admin\UserController@updateBuyer')->name('buyers.update');
                Route::get('delete/{id}', 'Admin\UserController@deleteBuyer')->name('buyers.delete');
            });

            Route::group(['prefix' => 'listings'], function () {
                Route::get('/', 'Admin\ListingController@listings')->name('listings.index');
                Route::get('edit/{id}', 'Admin\UserController@editBuyer')->name('buyers.edit');
                Route::patch('update/{id}', 'Admin\UserController@updateBuyer')->name('buyers.update');
                Route::get('delete/{id}', 'Admin\UserController@deleteBuyer')->name('buyers.delete');
            });
    
        });
    });
});

