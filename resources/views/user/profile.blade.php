@extends('layout.main')
@section('content')
<!--Profile-setting-->
<section class="user_profile inner_pages">
    <div class="container">
        <!-- <div class="user_profile_info gray-bg padding_4x4_40">
            <div class="upload_user_logo">
                <img src="assets/images/222x172.jpg" alt="image">
                <div class="upload_newlogo">
                    <input name="upload" type="file">
                </div>
            </div>
            <div class="dealer_info">
                <h5>Autospot Used Cars Center </h5>
                <p>P.1225 N Broadway Ave <br>
                    Oklahoma City, OK  1234-5678-090
                </p>
            </div>
        </div> -->
        <div class="row">
            <div class="col-md-3 col-sm-3">
                <div class="profile_nav">
                    <ul>
                        <li class="{{ Route::currentRouteName() == 'profile' ? 'active' : '' }}"><a href="{{ route('profile') }}">Profile Settings</a></li>
                        <li class="{{ Route::currentRouteName() == 'myListings' ? 'active' : '' }}"><a href="{{ route('myListings') }}">My Vehicles</a></li>
                        <li class="{{ Route::currentRouteName() == 'postListing' ? 'active' : '' }}"><a href="{{ route('postListing') }}">Post a Vehicles</a></li>
                        <li><a href="#">Sign Out</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6 col-sm-8">
                <div class="profile_wrap">
                    <h5 class="uppercase underline">Genral Settings</h5>
                    @if(session()->has('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{ route('update.profile', ['id' => $user->id]) }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                        <div class="form-group">
                            <label class="control-label">First Name</label>
                            <input class="form-control white_bg" id="fullname" name="firstname" type="text" value="{{ $user->detail->firstname }}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Last Name</label>
                            <input class="form-control white_bg" id="fullname" name="lastname" type="text" value="{{ $user->detail->lastname }}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Username</label>
                            <input class="form-control white_bg" id="email" name="username" type="text" value="{{ $user->username }}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Email Address</label>
                            <input class="form-control white_bg" id="email" name="email" type="email" value="{{ $user->email }}">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Mobile No.</label>
                            <input class="form-control white_bg" id="phone-number" name="mobile" type="text" value="{{ $user->detail->mobile }}">
                        </div>
                        <!-- <div class="form-group">
                            <label class="control-label">Date of Birth</label>
                            <input class="form-control white_bg" id="birth-date" type="text">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Your Address</label>
                            <textarea class="form-control white_bg" rows="4"></textarea>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Country</label>
                            <input class="form-control white_bg" id="country" type="text">
                        </div> -->
                        <!-- <div class="form-group">
                            <label class="control-label">City</label>
                            <input class="form-control white_bg" id="city" type="text">
                        </div> -->
                        <div class="gray-bg field-title">
                            <h6>Update password</h6>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Password</label>
                            <input class="form-control white_bg" id="password" type="password" name="password"> 
                        </div>
                        <!-- <div class="form-group">
                            <label class="control-label">Confirm Password</label>
                            <input class="form-control white_bg" id="c-password" type="password" name="password_confirmation">
                        </div> -->
                        <!-- <div class="gray-bg field-title">
                            <h6>Social Links</h6>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Facebook ID</label>
                            <input class="form-control white_bg" id="facebook" type="text">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Twitter ID</label>
                            <input class="form-control white_bg" id="twitter" type="text">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Linkedin ID</label>
                            <input class="form-control white_bg" id="linkedin" type="text">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Google+ ID</label>
                            <input class="form-control white_bg" id="google" type="text">
                        </div> -->
                        <div class="form-group">
                            <button type="submit" class="btn">Save Changes <span class="angle_arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></span></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/Profile-setting--> 
<!--Brands-->
<section class="brand-section gray-bg">
    <div class="container">
        <div class="brand-hadding">
            <h5>Popular Brands</h5>
        </div>
        <div class="brand-logo-list">
            <div id="popular_brands">
                <div><a href="#"><img src="assets/images/100x60.png" class="img-responsive" alt="image"></a></div>
                <div><a href="#"><img src="assets/images/100x60.png" class="img-responsive" alt="image"></a></div>
                <div><a href="#"><img src="assets/images/100x60.png" class="img-responsive" alt="image"></a></div>
                <div><a href="#"><img src="assets/images/100x60.png" class="img-responsive" alt="image"></a></div>
                <div><a href="#"><img src="assets/images/100x60.png" class="img-responsive" alt="image"></a></div>
                <div><a href="#"><img src="assets/images/100x60.png" class="img-responsive" alt="image"></a></div>
                <div><a href="#"><img src="assets/images/100x60.png" class="img-responsive" alt="image"></a></div>
            </div>
        </div>
    </div>
</section>
<!-- /Brands-->
@endsection