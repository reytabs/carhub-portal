@extends('layout.cms')
@section('content')
<section class="content-header">
    <h1>
        Edit Seller
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-edit"></i> Seller > edit</a></li>
        <!-- <li><a href="#">My Account</a></li> -->
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-8">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Form</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" action="{{ route('sellers.update', ['id' => $user->id]) }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                    <div class="box-body">
                        @if ($errors->any())
                            <div class="callout callout-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if(session()->has('success'))
                            <div class="callout callout-success">
                                {{ session('success') }}
                            </div>
                        @endif

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Firstname</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="firstname" value="{{ $user->detail->firstname }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Lastname</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="lastname" value="{{ $user->detail->lastname }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Username</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="username" value="{{ $user->username }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Email Address</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="email" value="{{ $user->email }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Mobile</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="mobile" value="{{ $user->detail->mobile }}">
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                    <button type="submit" class="btn btn-sm btn-primary pull-right">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection