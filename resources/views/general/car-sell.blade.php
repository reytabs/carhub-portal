@extends('layout.main')
@section('content')
<!--Page Header-->
<section class="page-header contactus_page">
    <div class="container">
        <div class="page-header_wrap">
            <div class="page-heading">
                <h1>Car Sell</h1>
            </div>
            <ul class="coustom-breadcrumb">
                <li><a href="#">Home</a></li>
                <li>Car Sell</li>
            </ul>
        </div>
    </div>
    <!-- Dark Overlay-->
    <div class="dark-overlay"></div>
</section>
<!-- /Page Header--> 
<!--Contact-us-->
<section class="contact_us section-padding">
    <div class="container">
        <div  class="row">
            <div class="col-md-6">
                <h3>Vehicle Details</h3>
                <!-- <div class="contact_form gray-bg"> -->
                    <form action="#" method="get">
                        <div class="form-group">
                            <label class="control-label">Vehicle No <span>*</span></label>
                            <input type="text" class="form-control white_bg" id="fullname">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Mileage</label>
                            <input type="email" class="form-control white_bg" id="emailaddress">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Asking Price</label>
                            <input type="text" class="form-control white_bg" id="phonenumber">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Purchase From</label>
                            <select class="form-control white_bg">
                                <option>Select</option>
                                <option>Authorized Agent</option>
                                <option>Parallel Importer</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Accident Free</label>
                            <select class="form-control white_bg">
                                <option>Select</option>
                                <option>Yes</option>
                                <option>No</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Condition</label>
                            <textarea class="form-control white_bg" rows="3" placeholder="e.g. Not drivable, air-con not cold, leather seats torn, major dent on rear bumper, minor accident on front bumper 1 year ago"></textarea>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Accessories/Features</label>
                            <textarea class="form-control white_bg" rows="3" placeholder="e.g. Leather seats, sports rims, sun-roof, 5 door model"></textarea>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Photos</label>
                            <input type="file" class="form-control-file white_bg">
                        </div>
                        <br>
                        <!-- <div class="form-group">
                            <button class="btn" type="submit">Send Message <span class="angle_arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></span></button>
                        </div> -->
                    </form>
                <!-- </div> -->
            </div>
            <div class="col-md-6">
                <h3>Owner Details</h3>
                <!-- <div class="contact_form gray-bg"> -->
                    <form action="#" method="get">
                        <div class="form-group">
                            <label class="control-label">ID Type</label>
                            <select class="form-control white_bg">
                                <option>Business (e.g. 51234567M)</option>
                                <option>Club/Assiciation/Organisation (e.g T08PQ1234A)</option>
                                <option>Company (e.g 198912235K)</option>
                                <option>Foreign Company (e.g T08PQ1234A)</option>
                                <option>Foreign Identification Number (e.g F/G1234567N)</option>
                                <option>Foreign Passport (e.g 12345678)</option>
                                <option>Government (e.g T08PQ1234A)</option>
                                <option>Limited Liabilty Partnership (e.g 198912235K)</option>
                                <option>Limited Partnership (e.g T08PQ1234A)</option>
                                <option>Malaysia NRIC (e.g 200312345678)</option>
                                <option>Professional (e.g T08PQ1234A)</option>
                                <option>Singapore NRIC (e.g S1234567D)</option>
                                <option>Statutory Board (e.g T08PQ1234A)</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">ID No.</label>
                            <input type="email" class="form-control white_bg" id="emailaddress">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Name <span>*</span></label>
                            <input type="text" class="form-control white_bg" id="phonenumber">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Mobile <span>*</span></label>
                            <textarea class="form-control white_bg" rows="4"></textarea>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Email</label>
                            <input type="email" class="form-control white_bg" id="emailaddress">
                        </div>
                        <!-- <div class="form-group">
                            <button class="btn" type="submit">Send Message <span class="angle_arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></span></button>
                        </div> -->
                    </form>
                <!-- </div> -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group pull-center">
                    <button class="btn" type="submit">Submit <span class="angle_arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></span></button>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /Contact-us--> 
<!--Brands-->
<section class="brand-section gray-bg">
    <div class="container">
        <div class="brand-hadding">
            <h5>Popular Brands</h5>
        </div>
        <div class="brand-logo-list">
            <div id="popular_brands">
                <div><a href="#"><img src="assets/images/100x60.png" class="img-responsive" alt="image"></a></div>
                <div><a href="#"><img src="assets/images/100x60.png" class="img-responsive" alt="image"></a></div>
                <div><a href="#"><img src="assets/images/100x60.png" class="img-responsive" alt="image"></a></div>
                <div><a href="#"><img src="assets/images/100x60.png" class="img-responsive" alt="image"></a></div>
                <div><a href="#"><img src="assets/images/100x60.png" class="img-responsive" alt="image"></a></div>
                <div><a href="#"><img src="assets/images/100x60.png" class="img-responsive" alt="image"></a></div>
                <div><a href="#"><img src="assets/images/100x60.png" class="img-responsive" alt="image"></a></div>
            </div>
        </div>
    </div>
</section>
<!-- /Brands-->
@endsection