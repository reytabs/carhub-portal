<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RegistrationRequest;
use App\User;
use App\UserDetail;
use App\Http\Requests\UpdateUserRequest;

class UserController extends Controller
{
    private $userModel;
    private $userDetailModel;

    public function __construct(User $userModel, UserDetail $userDetailModel)
    {
        $this->userModel = $userModel;
        $this->userDetailModel = $userDetailModel;
    }
    public function profile()
    {
        $user = \Auth::user();

        return view('user.profile', compact('user'));
    }

    public function myListings()
    {
        return view('user.my-vehicle');
    }

    public function postListing()
    {
        return view('user.post-vehicle');
    }

    public function registration(RegistrationRequest $request)
    {
        
        $user = \DB::transaction(function () use ($request) {
            $user = new $this->userModel;
            $user->username = $request->username;
            $user->email = $request->email;
            $user->password = \Hash::make($request->password);
            $user->save();


            $userDetail = new $this->userDetailModel;
            $userDetail->user_id = $user->id;
            $userDetail->firstname =  $request->firstname;
            $userDetail->lastname = $request->lastname;
            $userDetail->mobile = $request->mobile;
            $userDetail->save();

            $roleId = 3;
            if($request->role == 'seller')
                $roleId = 2;
    
            $user->roles()->attach($roleId);
        });

        return $user;
    }

    public function login(Request $request)
    {
        if (\Auth::attempt(['username' => $request->username, 'password' => $request->password])) {
            return ['success' => true, 'message' => 'Authenticated!'];
        } else {
            return ['success' => false, 'message' => 'Invalid username/password. Please enter an valid account!'];
        }
    }

    public function logout()
    {
        \Auth::logout();

        return redirect('/');
    }

    public function profileUpdate($id, Request $request)
    {
        $user = $this->userModel->find($id);
        $user->username = $request->username;
        $user->email = $request->email;
        if($request->password != null || $request->password != "")
            $user->password = \Hash::make($request->password);
        $user->save();


        $userDetail = $this->userDetailModel->where('user_id', $id)->first();
        $userDetail->user_id = $user->id;
        $userDetail->firstname =  $request->firstname;
        $userDetail->lastname = $request->lastname;
        $userDetail->mobile = $request->mobile;
        $userDetail->save();

        return redirect()->back()->with('success', 'Profile Succesfully updated.');
    }
}
