<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $fillable = [
        'brand', 'model', 'mileage', 'transmission', 'fuel_type', 'engine', 'seats', 'registered_year', 'ownership_license_in_years'
    ];

    public function seller()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
