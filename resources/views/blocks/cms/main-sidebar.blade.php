<div class="main-sidebar-container">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="{{ Request::is('dashboard') ? 'active' : null }}">
                <a href="/dashboard">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="{{ Request::is('sellers.index') ? 'active' : null }}">
                <a href="{{ route('sellers.index') }}">
                    <i class="fa fa-users"></i>
                    <span>Sellers</span>
                </a>
            </li>
            <li class="{{ Request::is('buyers.index') ? 'active' : null }}">
                <a href="{{ route('buyers.index') }}">
                    <i class="fa fa-user-plus"></i>
                    <span>Buyers</span>
                </a>
            </li>
            <li class="{{ Request::is('dashboard') ? 'active' : null }}">
                <a href="{{ route('listings.index') }}">
                    <i class="fa fa-car"></i>
                    <span>Listings</span>
                </a>
            </li>
            <li class="{{ Request::is('dashboard') ? 'active' : null }}">
                <a href="/dashboard">
                    <i class="fa fa-bar-chart-o"></i>
                    <span>Reports</span>
                </a>
            </li>
        </ul>

    </section>
    <!-- /.sidebar -->
</div>