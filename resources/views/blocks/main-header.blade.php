<!-- Navigation -->
<nav id="navigation_bar" class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <div class="logo"> <a href="index.html"><img src="assets/images/logo.png" alt="image"/></a> </div>
            <button id="menu_slide" data-target="#navigation" aria-expanded="false" data-toggle="collapse" class="navbar-toggle collapsed" type="button"> 
            <span class="sr-only">Toggle navigation</span> 
            <span class="icon-bar"></span> 
            <span class="icon-bar"></span> 
            <span class="icon-bar"></span> 
            </button>
        </div>
        <div class="collapse navbar-collapse" id="navigation">
            <ul class="nav navbar-nav">
                <li><a href="{{ route('general') }}">Home</a></li>
                <li><a href="{{ route('about-us') }}">About Us</a></li>
                <li><a href="{{ route('sell-car') }}">Sell Car</a></li>
                <li><a href="{{ route('listings') }}">Car Bid</a></li>
                <li><a href="{{ route('contact-us') }}">Contact Us</a></li>
            </ul>
        </div>
        <div class="header_wrap">
            @if(Auth::check())
            <div class="user_login">
                <ul>
                    <li class="dropdown">
                        <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user-circle" aria-hidden="true"></i></a>
                        <ul class="dropdown-menu">
                            <li class="{{ Route::currentRouteName() == 'profile' ? 'active' : '' }}"><a href="{{ route('profile') }}">Profile Settings</a></li>
                            <li class="{{ Route::currentRouteName() == 'myListings' ? 'active' : '' }}"><a href="{{ route('myListings') }}">My Vehicles</a></li>
                            <li class="{{ Route::currentRouteName() == 'postListing' ? 'active' : '' }}"><a href="{{ route('postListing') }}">Post a Vehicles</a></li>
                            <li><a href="#">Sign Out</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            @endif
            @if(!Auth::check())
                <div class="login_btn"> <a href="#loginform" class="btn btn-xs uppercase" data-toggle="modal" data-dismiss="modal">Login / Register</a> </div>
            @else
                <div class="login_btn"> <a href="{{ route('logout') }}" class="btn btn-xs uppercase" data-toggle="modal" data-dismiss="modal">Sign Out</a> </div>
            @endif
        </div>
    </div>
</nav>
<!-- Navigation end  -->