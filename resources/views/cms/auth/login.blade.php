<!DOCTYPE html>
<html>
<head>
    @include('blocks.cms.metadata')
    <script type="text/javascript">
    //<![CDATA[
    var APPLICATION_URL = "{{ url('/') }}";
    var CSRF_TOKEN      = "{{ csrf_token() }}";
    //]]>
  </script>
</head>
<body>
<div class="row">
    {{--<div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 align-middle">--}}
    <div class="login-box">
        <!-- /.login-logo -->
        <div class="login-box-body">
            <div class="login-logo">
                <a href="#"><b>Carhub</b>Portal</a>
            </div>
            <p class="login-box-msg">Sign in to start</p>
            @if(session()->has('login_message'))
                <div class="callout callout-danger">
                    <p>{{ session('login_message') }}</p>
                </div>
            @endif
            <form method="POST" class="form-w-loading" action="{{ route('post.login') }}">
                {{ csrf_field() }}
                <div class="form-group has-feedback">
                    <input type="text" name="username" class="form-control" placeholder="Username" value="">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" name="password" class="form-control" placeholder="Password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                {{-- <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox" name="remember"> Remember Me
                        </label>
                    </div>
                </div> --}}
                <!-- /.col -->
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-w-loading btn-primary btn-block btn-flat">Sign In</button>
                    </div>
                </div>
            </form>
            <br />
            <!-- <div class="row">
                <div class="col-xs-6 text-left">
                    <a href="">Forgot Password</a>
                </div>
                <div class="col-xs-6 text-right">
                    <a href="">Register</a>
                </div>
            </div> -->
        </div>
    </div>
</body>
@include('blocks.cms.assets')
</html>
